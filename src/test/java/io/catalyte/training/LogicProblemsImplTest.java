package io.catalyte.training;

import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class LogicProblemsImplTest {
  //TODO: Implement all requirements as specified in the requirements document

    LogicProblemsImpl exercise = new LogicProblemsImpl();
    @Test
    void testAverageArrayOfInt(){
        int[] test = {1,4,2};
        Double expected = 2.33;
        Double actual = exercise.average(test);
        assertEquals(expected, actual);
    }

    @Test
    void testAverageArrayOfSingleInt(){
        int[] test = {10};
        Double expected = 10.00;
        Double actual = exercise.average(test);
        assertEquals(expected, actual);
    }

    @Test
    void testAverageEmptyArray(){
        int[] test = {};
        Double expected = 0.00;
        Double actual = exercise.average(test);
        assertEquals(expected, actual);
    }
    @Test
    void testAverageNegativeArray(){
        int[] test = {1,2,3,-4};
       assertThrows(RuntimeException.class, () -> {exercise.average(test);});
    }

    @Test
    void testLastWordLengthGivenString(){
        String test = "input must not be an empty string";
        int expected = 6;
        int actual = exercise.lastWordLength(test);
        assertEquals(expected, actual);
    }

    @Test
    void testLastWordLengthGivenStringWithSpaceInTheEnd(){
        String test = "input must not be an empty string    ";
        int expected = 6;
        int actual = exercise.lastWordLength(test);
        assertEquals(expected, actual);
    }

    @Test
    void testLastWordLengthGivenStringWithSpaceInTheBeginning(){
        String test = "       input must not be an empty string";
        int expected = 6;
        int actual = exercise.lastWordLength(test);
        assertEquals(expected, actual);
    }
    @Test
    void testLastWordLengthGivenWhitespaceString(){
        String test = "        ";
        int expected = 0;
        int actual = exercise.lastWordLength(test);
        assertEquals(expected, actual);
    }

    @Test
    void testLastWordLengthGivenEmptyString(){
        String test = "";
        assertThrows(RuntimeException.class, () -> {exercise.lastWordLength(test);});
    }

    @Test
    void testDistinctLadderPathsGivenRungsThree(){
        int test = 3;
        BigDecimal expected = BigDecimal.valueOf(3);
        BigDecimal actual = exercise.distinctLadderPaths(test);
        assertEquals(expected, actual);
    }

    @Test
    void testDistinctLadderPathsGivenRungsOneHundreds(){
        int test = 100;
        BigDecimal expected = new BigDecimal("573147844013817084101");
        BigDecimal actual = exercise.distinctLadderPaths(test);
        assertEquals(expected, actual);
    }

    @Test
    void testDistinctLadderPathGivenRungsZero(){
        int test = 0;
        BigDecimal expected = BigDecimal.valueOf(0);
        BigDecimal actual = exercise.distinctLadderPaths(test);
        assertEquals(expected, actual);
    }

    @Test
    void testDistinctLadderPathGivenRungsNegative(){
        int test = -1;
        assertThrows(RuntimeException.class, () -> {exercise.distinctLadderPaths(test);});
    }

    @Test
    void testGroupStringGivenArrayOfString(){
        String[] test = {"arrange", "act", "assert", "ace","sky","dog"};
        List<List<String>> expected = new ArrayList<>();
        expected.add(List.of("act","assert"));
        expected.add(List.of("dog"));
        expected.add(List.of("arrange","ace"));
        expected.add(List.of("sky"));
        List<List<String>> actual = exercise.groupStrings(test);
        assertEquals(expected, actual);
    }

    @Test
    void testGroupStringGivenAnEmptyArray(){
        String[] test = {};
        List<List<String>> expected = new ArrayList<>();
        List<List<String>> actual = exercise.groupStrings(test);
        assertEquals(expected, actual);
    }

    @Test
    void testGroupStringGivenEmptyString(){
        String[] test = {""};
        assertThrows(RuntimeException.class, () -> {exercise.groupStrings(test);});
    }
}