/**
 * @author      CP-CO Alpha 1 -- Churina
 * @version     1.0
 * Description: Logic problems exercise
 */

package io.catalyte.training;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class LogicProblemsImpl implements LogicProblems {
  //TODO: Implement all requirements as defined in the project description.

    /**
     * This function will be used for calculating averages of assessment scores.
     *
     * @param scores the List of scores to be calculated.
     * @return  average of the given scores.
     */

      public Double average(int[] scores) {
        double average = 0;
          if (scores.length == 0) {return 0.00;} // if given a negative number, throw exception
          for (int score : scores) {

              // If given an empty array, return 0.00;
              if (score < 0) {
                  throw new RuntimeException("scores must be positive");
                  // if given an array of ints, return average
              } else {
                  average = Arrays.stream(scores).average().getAsDouble();
              }
              average = Double.parseDouble(new DecimalFormat("#0.00").format(average));
          }
        return average;
    } // end average class


    /**
     * This function will be used for returning the length of the last word in the string.
     *
     * @param text the List of Strings.
     * @return  The length of the last word.
     */

    @Override
    public int lastWordLength(String text) {
        String[] textArray = text.trim().split("\s");
        if(text.equals("\s")){return 0;}
        if(text.equals("")){
            throw new RuntimeException("input must not be an empty string");
        } else {
            return textArray[textArray.length-1].length();
        }
    } //end lastWordLength class


    /**
     * This function will be used for returning the number of distinct paths to reach the top of the ladder if given rungs number.
     *
     * @param rungs number.
     * @return  The number of distinct ladder paths.
     */

    @Override
    public BigDecimal distinctLadderPaths(int rungs) {
        // if given 0 rungs, return 0.
        if(rungs == 0){
            return BigDecimal.valueOf(0);
        }

        // if given negative numbers, then throw exception.
        if(rungs < 0){
            throw new RuntimeException("ladders can't have negative rungs");
        }

        // fibonacci serious: 1,1,2,3,5,8,13,21...... (skip first 0).
        // the 100th number in requirement is actually 101th, so create an array with "rungs+1".

        // declare and initialize a dynamic array to hold rung numbers.
        BigDecimal[] arr = new BigDecimal[rungs+1];

        // set a base number. since the values of index 0 (1 rung) and index 1 ( 2 rungs) are constant, set them to 1.
        arr[0] = BigDecimal.valueOf(1);
        arr[1] = BigDecimal.valueOf(1);

        // loop through the array to calculate the distinct ladder paths.
        for(int i = 2; i < arr.length; i++){
            arr[i] = arr[i - 2].add(arr[i - 1]);
        }
        return arr[rungs];
    } // end distinctLadderPaths class


    /**
     * If given an array of strings,this function will return a List of Lists that each contain properly grouped strings.
     *
     * @param strs an array of strings.
     * @return a List of Lists that each contain properly grouped strings.
     */

    @Override
    public List<List<String>> groupStrings(String[] strs) {
        HashMap<String, ArrayList<String>> groupString = new HashMap<>();

        // loop through the given string,generate keys and values to create a hashmap.
        for (String string : strs) {

            // if given an empty array, throw an exception.
            if(string.equals("")){
                throw new RuntimeException("strings must not be empty");
            }

            // extract first and last letter of each string to get keys of strings.
            String key = string.charAt(0) + String.valueOf(string.charAt(string.length() - 1));
            //String key = string.substring(0,1) + string.substring(string.length() - 1);

            // if the map contain this key, add the string to the map.
            if(groupString.containsKey(key)){
                groupString.get(key).add(string);
            } else {
                // if the map doesn't contain this key, create a new ArrayList, put it in the map.
                groupString.put(key, new ArrayList<>());
                groupString.get(key).add(string);
            }
        }
            // convert map value to required list type.
        return new ArrayList<>(groupString.values());
    }
} // end groupStrings class
