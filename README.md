# Intermediate Programming Project Template

Intermediate Programming Project Template is used to give trainees a base project for demonstrating logical thinking. 

## Installation

Fork the repository to your specified work directory, then clone it down

```bash
git clone <insert_url_here>
```

## Usage
Fill out LogicProblemsImpl with working solutions to the interface. Follow all requirements specified in the project instructions.

## Contributing
Feedback is welcome. If you see issues, or have ideas for improvement, please submit feedback!

## Description
This project is a Java logic problem exercise. It gives the solutions to the problems based on the given criteria.

## How to run the test
A number of unit tests have been written to test the implementation of each method.

To test your solutions:

Make sure the test/java folder is Marked as the Test Sources Root (java folder should be green). If not, right-click on the java folder and click Mark Directory as -> Test Sources Root.
Open the LogicProblemsImplTest class and right-click on the test of the method you are trying to test.
A dialog should appear and indicate whether the test passed or failed.
To run all the tests, you can right-click on the LogicProblemsImplTest class and click Run LogicProblemsImplTest.
Right-click on the LogicProblemsImplTest class, then choose Run LogicProblemsImplTest with Coverage to monitor the test coverage.

## How the lint
Please refer to Google Java Style Guide: https://google.github.io/styleguide/javaguide.html